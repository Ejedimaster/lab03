public class Cat 
{
	
	public String breed;
	public int age;
	public String name;
	
	public void speak()
	{
		System.out.println(name + " says hi!");
	}
	
	public void whatBreed()
	{
		System.out.println(name + " is a " + breed);
	}
}